﻿using System;

namespace MeterialInventory
{
    class Program
    {
        static void Main(string[] args)
        {
            string line;
            Inventory inventory = new Inventory();
            while ((line = Console.ReadLine()) != null)
            {
                if (line.Length == 0 || line[0] == '#')
                    continue;

                inventory.ParseLine(line);
            }
            inventory.Sort();
            inventory.PrintInventory();
            Console.ReadKey();
        }
    }
}
