﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MeterialInventory
{
    class Storage : IComparable<Storage>
    {
        public String Name { get; set; }

        public int TotalCount { get; private set; } = 0;
        private Dictionary<Material, int> _materials = new Dictionary<Material, int>();

        public void Add(Material material, int count)
        {
            if (!_materials.ContainsKey(material))
                _materials[material] = count;
            else
                _materials[material] += count;
            
            TotalCount += count;
        }

        public void PrintSorted()
        {
            Console.WriteLine("{0} (total {1})", Name, TotalCount);
            foreach (var materialStash in _materials.OrderBy(x => x.Key))
            {
                Console.WriteLine("{0}: {1}", materialStash.Key.Id, materialStash.Value);
            }
        }

        #region IComparable method
        public int CompareTo(Storage other)
        {
            int res = other.TotalCount.CompareTo(TotalCount);
            if (res == 0)
                res = other.Name.CompareTo(Name);

            return res;
        }
        #endregion
    }
}
