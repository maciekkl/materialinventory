﻿using System;

namespace MeterialInventory
{
    class Material : IComparable<Material>
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public Material(string name, string id)
        {
            Name = name;
            Id = id;
        }

        #region IComparable method
        public int CompareTo(Material other)
        {
            return Id.CompareTo(other.Id);
        }
        #endregion
    }
}
