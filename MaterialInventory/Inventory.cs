﻿using System;
using System.Collections.Generic;

namespace MeterialInventory
{
    class Inventory
    {
        private List<Storage> _storages = new List<Storage>();

        /// <summary>
        /// Parses single line from raw material inventory
        /// </summary>
        /// <param name="materialInput"></param>
        /// <exception cref="FormatException">Input string does not match proper format</exception>
        /// <exception cref="OverflowException">material count out of range</exception>
        public void ParseLine(string inputLine)
        {
            string[] split = inputLine.Split(';');
            if (split.Length < 3)
                throw new FormatException("Invalid line format");

            Material material = new Material(split[0], split[1]);
            string[] materialDistribution = split[2].Split('|');
            foreach (string distribution in materialDistribution)
            {
                string[] storageInput = distribution.Split(',');
                if (storageInput.Length < 2)
                    throw new FormatException("Invalid input format for storage");

                int count = Convert.ToInt32(storageInput[1]);
                AddToStorage(storageInput[0], material, count);
            }
        }

        public void Sort()
        {
            _storages.Sort();
        }

        public void PrintInventory()
        {
            bool first = true;
            foreach (Storage storage in _storages)
            {
                if (first)
                    first = false;
                else
                    Console.WriteLine();

                storage.PrintSorted();
            }
        }

        private void AddToStorage(string storageName, Material material, int count)
        {
            Storage storage = _storages.Find(x => x.Name == storageName);
            if (storage == null)
            {
                storage = new Storage() { Name = storageName };
                _storages.Add(storage);
            }

            storage.Add(material, count);
        }


    }
}
